#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/records.h>
#include <openssl/symbolic.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <getopt.h>
///////////

///////////
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

unsigned char cookie_secret[32];
static char *psk_identity = "Client_identity";
static char *psk_key = "1234";
char dir_address[30] = "";
const unsigned char tls13_aes128gcmsha256_id[] = {0x13, 0x01};
static char *ciphersuite = "PSK-AES128-CBC-SHA";
bool dump_mode = 0;
bool isFragmented = 0;

uint8_t CH0_buffer[CH0_size];
uint8_t CH2_buffer[CH2_size];
// Fragmentation
uint8_t CKE_buffer[CKE_unfrag_size];
uint8_t CKE_bufferfrag[CKE_frag_size];
uint8_t SH_buffer[SH_unfrag_size];
uint8_t SH_bufferfrag[SH_frag_size];
//
uint8_t CCC_buffer[CCS_size];
uint8_t CFI_buffer[FIN_size];
uint8_t HVR_buffer[HVR_size];
uint8_t SHD_buffer[SHD_size];
uint8_t SCC_buffer[CCS_size];
uint8_t SFI_buffer[FIN_size];
/////
CH0 client_hello1;
CH0 shadow_client_hello1;
CH2 client_hello2;
CH2 shadow_client_hello2;
CKE client_key_exchange;
CKE shadow_client_key_exchange;
CKEFRAG client_key_exchange_frag;
CKEFRAG shadow_client_key_exchange_frag;
HVR hello_verify_request;
HVR shadow_hello_verify_request;
SH server_hello;
SH shadow_server_hello;
SHFRAG server_hello_frag;
SHFRAG shadow_server_hello_frag;
SHD server_hello_done;
SHD shadow_server_hello_done;
CCS client_change_cipher;
CCS shadow_client_change_cipher;
FIN client_finished;
FIN shadow_client_finished;
CCS server_change_cipher;
CCS shadow_server_change_cipher;
FIN server_finished;
FIN shadow_server_finished;
///

void err()
{
    ERR_print_errors_fp(stderr);
    exit(1);
}

static unsigned int psk_server_cb(SSL *ssl, const char *identity,
                                  unsigned char *psk,
                                  unsigned int max_psk_len)
{
    long key_len = 0;
    BIGNUM *bn = NULL;
    int ret;

    if (identity == NULL)
    {
        err();
    }
    /* here we could lookup the given identity e.g. from a database */
    if (strcmp(identity, psk_identity) != 0)
    {
        err();
    }
    else
    {
        printf("PSK client identity found\n");
    }

    /* convert the PSK key to binary */
    ret = BN_hex2bn(&bn, psk_key);
    if (!ret)
    {
        err();
    }

    if (BN_num_bytes(bn) > (int)max_psk_len)
    {
        err();
    }

    ret = BN_bn2bin(bn, psk);
    BN_free(bn);
    if (ret < 0)
    {
        err();
    }
    key_len = (unsigned int)ret;

    printf("fetched PSK len=%ld\n", key_len);
    return key_len;
}

/*
static int psk_find_session_cb(SSL *ssl, const unsigned char *identity,
                               size_t identity_len, SSL_SESSION **sess)
{
    SSL_SESSION *tmpsess = NULL;
    unsigned char *key;
    long key_len;
    const SSL_CIPHER *cipher = NULL;

    if (strlen(psk_identity) != identity_len || memcmp(psk_identity, identity, identity_len) != 0)
    {
        *sess = NULL;
        return 1;
    }

    key = OPENSSL_hexstr2buf(psk_key, &key_len);
    if (key == NULL)
    {
        err();
    }

    cipher = SSL_CIPHER_find(ssl, tls13_aes128gcmsha256_id);
    if (cipher == NULL)
    {
        err();
    }

    tmpsess = SSL_SESSION_new();
    if (tmpsess == NULL || !SSL_SESSION_set1_master_key(tmpsess, key, key_len) || !SSL_SESSION_set_cipher(tmpsess, cipher) || !SSL_SESSION_set_protocol_version(tmpsess, SSL_version(ssl)))
    {
        OPENSSL_free(key);
        return 0;
    }
    OPENSSL_free(key);
    *sess = tmpsess;

    return 1;
}
*/

static unsigned int psk_client_cb(SSL *ssl, const char *hint, char *identity,
                                  unsigned int max_identity_len,
                                  unsigned char *psk,
                                  unsigned int max_psk_len)
{
    long key_len;
    int ret;
    BIGNUM *bn = NULL;

    /*
     * lookup PSK identity and PSK key based on the given identity hint here
     */
    BIO_snprintf(identity, max_identity_len, "%s", psk_identity);

    /* convert the PSK key to binary */
    ret = BN_hex2bn(&bn, psk_key);
    if (!ret)
    {
        err();
    }

    key_len = BN_bn2bin(bn, psk);
    BN_free(bn);

    return key_len;
}

/*
static int psk_use_session_cb(SSL *s, const EVP_MD *md,
                              const unsigned char **id, size_t *idlen,
                              SSL_SESSION **sess)
{
    SSL_SESSION *usesess = NULL;
    const SSL_CIPHER *cipher = NULL;

    long key_len;
    unsigned char *key = OPENSSL_hexstr2buf(psk_key, &key_len);

    if (key == NULL)
    {
        err();
    }

    cipher = SSL_CIPHER_find(s, tls13_aes128gcmsha256_id);
    if (cipher == NULL)
    {
        err();
    }

    usesess = SSL_SESSION_new();
    if (usesess == NULL || !SSL_SESSION_set1_master_key(usesess, key, key_len) || !SSL_SESSION_set_cipher(usesess, cipher) || !SSL_SESSION_set_protocol_version(usesess, TLS1_3_VERSION))
    {
        OPENSSL_free(key);
        err();
    }
    OPENSSL_free(key);

    cipher = SSL_SESSION_get0_cipher(usesess);
    if (cipher == NULL)
        err();

    if (md != NULL && SSL_CIPHER_get_handshake_digest(cipher) != md)
    {

        *id = NULL;
        *idlen = 0;
        *sess = NULL;
        SSL_SESSION_free(usesess);
    }
    else
    {
        *sess = usesess;
        *id = (unsigned char *)psk_identity;
        *idlen = strlen(psk_identity);
    }

    return 1;
}
*/

int generate_cookie(SSL *ssl, unsigned char *cookie, unsigned int *cookie_len)
{
    unsigned char buffer[16], result[EVP_MAX_MD_SIZE];
    unsigned int length = 0, resultlength;

    memset(buffer, 1, 16);

    RAND_bytes(cookie_secret, 32);
    for (int i = 0; i < 32; i++)
    {
        cookie_secret[i] = 1;
    }
    /* Calculate HMAC of buffer using the secret */
    HMAC(EVP_sha1(), (const void *)cookie_secret, 32,
         (const unsigned char *)buffer, length, result, &resultlength);

    memcpy(cookie, result, resultlength);
    *cookie_len = resultlength;

    return 1;
}

int verify_cookie(SSL *ssl, unsigned char *cookie, unsigned int cookie_len)
{
    unsigned char buffer[16], result[EVP_MAX_MD_SIZE];
    unsigned int length = 0, resultlength;

    memset(buffer, 1, 16);

    HMAC(EVP_sha1(), (const void *)cookie_secret, 32,
         (const unsigned char *)buffer, length, result, &resultlength);

    if (cookie_len == resultlength && memcmp(result, cookie, resultlength) == 0)
        return 1;

    return 0;
}

/*
int config_ctx(SSL_CONF_CTX *cctx, STACK_OF(OPENSSL_STRING) * str,
               SSL_CTX *ctx)
{
    int i;

    SSL_CONF_CTX_set_ssl_ctx(cctx, ctx);
    for (i = 0; i < sk_OPENSSL_STRING_num(str); i += 2)
    {
        const char *flag = sk_OPENSSL_STRING_value(str, i);
        const char *arg = sk_OPENSSL_STRING_value(str, i + 1);
        if (SSL_CONF_cmd(cctx, flag, arg) <= 0)
        {
            if (arg != NULL)
                printf("Error with command: \"%s %s\"\n",
                       flag, arg);
            else
                printf("Error with command: \"%s\"\n", flag);
            return 0;
        }
    }
    if (!SSL_CONF_CTX_finish(cctx))
    {
        puts("Error finishing context\n");
        return 0;
    }
    return 1;
}
*/

void printusage(char *argv)
{
    puts("\nFor help:");
    printf("Usage: %s [-h]\n\n", argv);
    puts("\nFor Symbolic Constraint Testing:");
    printf("Usage: %s [-i DIR] [-r constraint] [-f] \n\n", argv);
    puts("For Handshake Packet Generation:");
    printf("Usage: %s [-n] \n\n", argv);

    puts("    Constraint:\t\t\t\tDescription:");
    puts("----------------------------------------------------------------------\n");
    puts("[+] The List of Record-level Constraints:\n");

    puts("[*] ctypevalidity-server\t\tContent Type Validity for the server side");
    puts("[*] ctypevalidity-client\t\tContent Type Validity for the client side");
    puts("[*] recversion-server\t\t\tRecord Version Validity for the server side");
    puts("[*] recversion-client\t\t\tRecord Version Validity for the client side");
    puts("[*] epoch-server\t\t\tEpoch Validity for the server side");
    puts("[*] epoch-client\t\t\tEpoch Validity for the client side");
    puts("[*] rsequniqueness-server\t\tRecord Sequence Uniqueness for the server side");
    puts("[*] rsequniqueness-client\t\tRecord Sequence Uniqueness for the client side");
    puts("[*] rseqwindowing-server\t\tRecord Sequence Windowing for the server side");
    puts("[*] rseqwindowing-client\t\tRecord Sequence Windowing for the client side");
    puts("[*] rlenvalidity-server\t\t\tRecord Length Validity for the server side");
    puts("[*] rlenvalidity-client\t\t\tRecord Length Validity for the client side");

    puts("\n[+] The List of Fragment-level Constraints:\n");

    puts("[*] htypevalidity-server\t\tHandshake Type Validity for the server side");
    puts("[*] htypevalidity-client\t\tHandshake Type Validity for the client side");
    puts("[*] mlenvalidity-server\t\t\tMessage Length Validity for the server side");
    puts("[*] mlenvalidity-client\t\t\tMessage Length Validity for the client side");
    puts("[*] mseqvalidity-server\t\t\tMessage Sequence Validity for the server side");
    puts("[*] mseqvalidity-client\t\t\tMessage Sequence Validity for the client side");
    puts("[*] flenvalidity-server\t\t\tFragment Length Validity for the server side");
    puts("[*] flenvalidity-client\t\t\tFragment Length Validity for the client side");
    puts("[*] fragoffset-server\t\t\tFragment Offset Validity for the server side");
    puts("[*] fragoffset-client\t\t\tFragment Offset Validity for the client side");
    puts("[*] flenmleneq-server\t\t\tEquality of Fragment Length and Message Length for the server side");
    puts("[*] flenmleneq-client\t\t\tEquality of Fragment Length and Message Length for the client side");
    puts("[*] bytecontain-server\t\t\tIs Fragmentation done Correctly for the server side");
    puts("[*] bytecontain-client\t\t\tIs Fragmentation done Correctly for the client side");
    puts("[*] mseqeq-server\t\t\tMessage Sequence Equality in all Fragments of the same message for the server side");
    puts("[*] mseqeq-client\t\t\tMessage Sequence Equality in all Fragments of the same message for the client side");
    puts("[*] mleneq-server\t\t\tMessage Length Equality in all Fragments of the same message for the server side");
    puts("[*] mleneq-client\t\t\tMessage Length Equality in all Fragments of the same message for the client side");

    puts("\n[+] The List of Message-level Constraints:\n");

    puts("[*] handversion-server\t\t\tHandshake Version Validity for the server side");
    puts("[*] handversion-client\t\t\tHandshake Version Validity for the client side");
    puts("[*] cookielenvalidity\t\t\tCookie Length Validity for the server side");
    puts("[*] cookielenvalidity\t\t\tCookie Length Validity for the client side");
    puts("[*] sessionidlenvalidity-server\t\tSession ID Length Validity for the server side");
    puts("[*] sessionidlenvalidity-client\t\tSession ID Length Validity for the client side");
    puts("[*] extlenvalidity-server\t\tExtension Length Validity for the server side");
    puts("[*] extlenvalidity-client\t\tExtension Length Validity for the client side");

    exit(-1);
}

int load_psk_from_file(char *rule)
{
    int ret;
    char file_name[100];

    sprintf(file_name, "%s/%s", dir_address, "0");
    ret = load_record(file_name, CH0_buffer, CH0_size);
    if (ret == -1)
        return -1;
    puts("[+] Client Hello 1 is loaded successfully");
    ////
    sprintf(file_name, "%s/%s", dir_address, "1");
    ret = load_record(file_name, HVR_buffer, HVR_size);
    if (ret == -1)
        return -1;
    puts("[+] Hello Verify Request is loaded successfully");
    ////
    sprintf(file_name, "%s/%s", dir_address, "2");
    ret = load_record(file_name, CH2_buffer, CH2_size);
    if (ret == -1)
        return -1;
    puts("[+] Client Hello 2 is loaded successfully");
    ////
    sprintf(file_name, "%s/%s", dir_address, "4");
    ret = load_record(file_name, SHD_buffer, SHD_size);
    if (ret == -1)
        return -1;
    puts("[+] Server Hello Done is loaded successfully");
    ////

    // Fragmentation
    if (isFragmented)
    {
        sprintf(file_name, "%s/%s", dir_address, "5");
        ret = load_record(file_name, CKE_bufferfrag, CKE_frag_size);
        if (ret == -1)
            return -1;

        puts("[+] Client Key Exchange Fragmented is loaded successfully");

        sprintf(file_name, "%s/%s", dir_address, "3");
        ret = load_record(file_name, SH_bufferfrag, SH_frag_size);
        if (ret == -1)
            return -1;

        puts("[+] Server Hello Fragmented is loaded successfully");
    }
    else
    {
        sprintf(file_name, "%s/%s", dir_address, "5");
        ret = load_record(file_name, CKE_buffer, CKE_unfrag_size);
        if (ret == -1)
            return -1;

        puts("[+] Client Key Exchange is loaded successfully");

        sprintf(file_name, "%s/%s", dir_address, "3");
        ret = load_record(file_name, SH_buffer, SH_unfrag_size);
        if (ret == -1)
            return -1;
        puts("[+] Server Hello is loaded successfully");
    }

    ////
    sprintf(file_name, "%s/%s", dir_address, "6");
    ret = load_record(file_name, CCC_buffer, CCS_size);
    if (ret == -1)
        return -1;
    puts("[+] Client Change Cipher Spec is loaded successfully");
    ////
    sprintf(file_name, "%s/%s", dir_address, "7");
    ret = load_record(file_name, CFI_buffer, FIN_size);
    if (ret == -1)
        return -1;
    puts("[+] Client Finished is loaded successfully");
    ////
    sprintf(file_name, "%s/%s", dir_address, "8");
    ret = load_record(file_name, SCC_buffer, CCS_size);
    if (ret == -1)
        return -1;
    puts("[+] Server Change Cipher Spec is loaded successfully");
    ////
    sprintf(file_name, "%s/%s", dir_address, "9");
    ret = load_record(file_name, SFI_buffer, FIN_size);
    if (ret == -1)
        return -1;
    puts("[+] Server Finished is loaded successfully");
    /////////
    parse_record(CH0_buffer, &client_hello1);
    shadow_client_hello1 = client_hello1;

    parse_record(CH2_buffer, &client_hello2);
    shadow_client_hello2 = client_hello2;

    // Fragmentation Parse
    if (isFragmented)
    {
        parse_record(CKE_bufferfrag, &client_key_exchange_frag);
        shadow_client_key_exchange_frag = client_key_exchange_frag;

        parse_record(SH_bufferfrag, &server_hello_frag);
        shadow_server_hello_frag = server_hello_frag;
    }
    else
    {
        parse_record(CKE_buffer, &client_key_exchange);
        shadow_client_key_exchange = client_key_exchange;

        parse_record(SH_buffer, &server_hello);
        shadow_server_hello = server_hello;
    }

    parse_record(CCC_buffer, &client_change_cipher);
    shadow_client_change_cipher = client_change_cipher;

    parse_record(CFI_buffer, &client_finished);
    shadow_client_finished = client_finished;
    //
    parse_record(HVR_buffer, &hello_verify_request);
    shadow_hello_verify_request = hello_verify_request;

    parse_record(SHD_buffer, &server_hello_done);
    shadow_server_hello_done = server_hello_done;

    parse_record(SCC_buffer, &server_change_cipher);
    shadow_server_change_cipher = server_change_cipher;

    parse_record(SFI_buffer, &server_finished);
    shadow_server_finished = server_finished;
    ///
#if ISSYM

    //////////////////// Record-Level Constraints

    if (strcmp(rule, "ctypevalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("ContentType Server Rule:\n\n");
        contentTypeServer(&client_hello1, &client_hello2, &client_key_exchange, &client_change_cipher);
    }
    else if (strcmp(rule, "ctypevalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("ContentType Client Rule:\n\n");
        contentTypeClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
    }
    else if (strcmp(rule, "recversion-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Record Version Server Rule:\n\n");
        recordVersionServer(&client_hello2, &client_key_exchange, &client_change_cipher);
    }
    else if (strcmp(rule, "recversion-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Record Version Client Rule:\n\n");
        recordVersionClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
    }
    else if (strcmp(rule, "epoch-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Epoch Rule for the Server Side:\n\n");
        epochServer(&client_hello1, &client_hello2, &client_key_exchange, &client_change_cipher);
    }
    else if (strcmp(rule, "epoch-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Epoch Rule for the Client Side:\n\n");
        epochClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
    }
    else if (strcmp(rule, "rsequniqueness-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Record Sequence Number Rule 2 for the Server Side:\n\n");
        sequenceUniquenessServer(&client_hello2, &client_key_exchange, &client_change_cipher);
    }
    else if (strcmp(rule, "rsequniqueness-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Record Sequence Number Rule 2 for the Client Side:\n\n");
        sequenceUniquenessClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
    }
    else if (strcmp(rule, "rseqwindowing-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Record Sequence Number Rule 3 for the Server Side:\n\n");
        sequenceWindowServer(&client_hello2, &client_key_exchange, &client_change_cipher);
    }
    else if (strcmp(rule, "rseqwindowing-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Record Sequence Number Rule 3 for the Client Side:\n\n");
        sequenceWindowClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
    }
    else if (strcmp(rule, "rlenvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Record Length Validity Server Rule:\n\n");
        recordLengthValidityServer(&client_hello1, &client_hello2, &client_key_exchange, &client_change_cipher,
                                   &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange, &shadow_client_change_cipher);
    }
    else if (strcmp(rule, "rlenvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Record Length Validity Client Rule:\n\n");
        recordLengthValidityClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher,
                                   &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done, &shadow_server_change_cipher);
    }
    ///////////////////////// Fragment-Level Constraints

    else if (strcmp(rule, "htypevalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Handshake Type Server Rule:\n\n");
        handshakeTypeValidityServer(&client_hello1, &client_hello2, &client_key_exchange,
                                    &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange);
    }

    else if (strcmp(rule, "htypevalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Handshake Type Client Rule:\n\n");
        handshakeTypeValidityClient(&hello_verify_request, &server_hello, &server_hello_done,
                                    &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done);
    }

    else if (strcmp(rule, "mlenvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Message Length Validity Server Rule:\n\n");
        messageLengthValidityServer(&client_hello1, &client_hello2, &client_key_exchange,
                                    &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange);
    }
    else if (strcmp(rule, "mlenvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Message Length Validity Client Rule:\n\n");
        messageLengthValidityClient(&hello_verify_request, &server_hello, &server_hello_done,
                                    &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done);
    }
    else if (strcmp(rule, "mseqvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Message Sequence Number Rule for the Server Side:\n\n");
        messageSequenceServer(&client_hello1, &client_hello2, &client_key_exchange);
    }
    else if (strcmp(rule, "mseqvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Message Sequence Number Rule for the Client Side:\n\n");
        messageSequenceClient(&hello_verify_request, &server_hello, &server_hello_done);
    }
    else if (strcmp(rule, "flenvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Fragment Length Validity Server Rule:\n\n");
        fragmentLengthValidityServer(&client_hello1, &client_hello2, &client_key_exchange,
                                     &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange);
    }
    else if (strcmp(rule, "flenvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Fragment Length Validity Client Rule:\n\n");
        fragmentLengthValidityClient(&hello_verify_request, &server_hello, &server_hello_done,
                                     &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done);
    }
    else if (strcmp(rule, "fragoffset-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragment Offset Validity Server Rule:\n");
        offsetValidityNoFragServer(&client_hello1, &client_hello2, &client_key_exchange);
    }
    else if (strcmp(rule, "fragoffset-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragment Offset Validity Client Rule:\n");
        offsetValidityNoFragClient(&hello_verify_request, &server_hello, &server_hello_done);
    }
    else if (strcmp(rule, "flenmleneq-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragment Length Message Length Equality Server Rule:\n");
        fragLenMessageLenEqualityServer(&client_hello1, &client_hello2, &client_key_exchange);
    }
    else if (strcmp(rule, "flenmleneq-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragment Length Message Length Equality Client Rule:\n");
        fragLenMessageLenEqualityClient(&hello_verify_request, &server_hello, &server_hello_done);
    }
    else if (strcmp(rule, "bytecontain-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Byte contained Server:\n");
        byteContainedFragServer(&client_key_exchange_frag);
    }
    else if (strcmp(rule, "bytecontain-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Byte contained Client:\n");
        byteContainedFragClient(&server_hello_frag);
    }
    else if (strcmp(rule, "mseqeq-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Message Sequence Equality In Both Fragments Server:\n");
        messageSequenceEqualityFragServer(&client_key_exchange_frag);
    }
    else if (strcmp(rule, "mseqeq-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Message Sequence Equality In Both Fragments Client:\n");
        messageSequenceEqualityFragClient(&server_hello_frag);
    }
    else if (strcmp(rule, "mleneq-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Message Length Equality In Both Fragments Server:\n");
        MessageLengthFragEqualityServer(&client_key_exchange_frag);
    }
    else if (strcmp(rule, "mleneq-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Message Length Equality In Both Fragments Client:\n");
        MessageLengthFragEqualityClient(&server_hello_frag);
    }
    /////////////////////// Message-Level Constraints
    else if (strcmp(rule, "handversion-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Handshake Version Server Rule:\n\n");
        handshakeVersionServer(&client_hello1, &client_hello2);
    }
    else if (strcmp(rule, "handversion-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Handshake Version Client Rule:\n\n");
        handshakeVersionClient(&hello_verify_request, &server_hello);
    }

    else if (strcmp(rule, "cookielenvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Cookie Length Rule Server:\n\n");
        cookieLengthServer(&client_hello1, &client_hello2, &shadow_client_hello1,
                           &shadow_client_hello2);
    }
    else if (strcmp(rule, "cookielenvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Cookie Length Rule Client:\n\n");
        cookieLengthClient(&hello_verify_request, &shadow_hello_verify_request);
    }
    else if (strcmp(rule, "sessionidlenvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("SessionID Rule Server:\n\n");
        sessionIDLengthServer(&client_hello1, &client_hello2, &shadow_client_hello1,
                              &shadow_client_hello2);
    }
    else if (strcmp(rule, "sessionidlenvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("SessionID Rule Client:\n\n");
        sessionIDLengthClient(&server_hello, &shadow_server_hello);
    }
    else if (strcmp(rule, "extlenvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Extension Length Validity Server:\n\n");
        extensionLengthValidityServer(&client_hello1, &client_hello2, &shadow_client_hello1,
                                      &shadow_client_hello2);
    }
    else if (strcmp(rule, "extlenvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Extension Length Validity client:\n\n");
        extensionLengthValidityClient(&server_hello, &shadow_server_hello);
    }
    /*
    else if (strcmp(rule, "rule7-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Fragment Length Record Length Relation Server Rule:\n\n");
        fragmentLenRecordLenServer(&client_hello1, &client_hello2, &client_key_exchange);
    }
    else if (strcmp(rule, "rule7-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Fragment Length Record Length Relation Client Rule:\n\n");
        fragmentLenRecordLenClient(&hello_verify_request, &server_hello, &server_hello_done);
    }
    else if (strcmp(rule, "rule8-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Message Length Record Length Relation Server Rule:\n\n");
        messageLenRecordlenServer(&client_hello1, &client_hello2, &client_key_exchange);
    }
    else if (strcmp(rule, "rule8-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Message Length Record Length Relation Client Rule:\n\n");
        messageLenRecordlenClient(&hello_verify_request, &server_hello, &server_hello_done);
    }
    else if (strcmp(rule, "rule17-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Fragment Offset Validity Server:\n");
        offsetValidityFragServer(&client_key_exchange_frag, &shadow_client_key_exchange_frag);
    }
    else if (strcmp(rule, "rule17-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Fragment Offset Validity Client:\n");
        offsetValidityFragClient(&server_hello_frag, &shadow_server_hello_frag);
    }
    
    else if (strcmp(rule, "rule21-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Fragment Length Message Length Relation Server:\n");
        fragLenMessageLenRelationServer(&client_key_exchange_frag);
    }
    else if (strcmp(rule, "rule21-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Fragmentat Length Message Length Relation Client:\n");
        fragLenMessageLenRelationClient(&server_hello_frag);
    }
    else if (strcmp(rule, "all-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Performing all experiments simultaneously Server-Side:\n\n\n\n");
        allConstraintsServer(&client_hello1, &client_hello2, &client_key_exchange, &client_change_cipher,
                             &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange, &shadow_client_change_cipher);
    }
    else if (strcmp(rule, "all-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Performing all experiments simultaneously Client-Side:\n\n\n\n");
        allConstraintsClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher,
                             &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done, &shadow_server_change_cipher);
    }
    */
    else if (strcmp(rule, "CH0") == 0)
    {
        experimenttype = SERVER;
        approach = SYMBEX;
        puts("Symbolically Executing ClientHello:\n");
        symbolicClientHello1(&client_hello1, &shadow_client_hello1);
    }
    else if (strcmp(rule, "CH2") == 0)
    {
        experimenttype = SERVER;
        approach = SYMBEX;
        puts("Symbolically Executing ClientHello2:\n");
        symbolicClientHello2(&client_hello2, &shadow_client_hello2);
    }
    else if (strcmp(rule, "CKE") == 0)
    {
        experimenttype = SERVER;
        approach = SYMBEX;
        puts("Symbolically Executing ClientKeyExchange:\n");
        symbolicClientKeyExchange(&client_key_exchange, &shadow_client_key_exchange);
    }
    else if (strcmp(rule, "CCC") == 0)
    {
        experimenttype = SERVER;
        approach = SYMBEX;
        puts("Symbolically Executing ChangeCipherSuite(S):\n");
        symbolicClientChangeCipher(&client_change_cipher, &shadow_client_change_cipher);
    }
    else if (strcmp(rule, "CFI") == 0)
    {
        experimenttype = SERVER;
        approach = SYMBEX;
        puts("Symbolically Executing Finished(S):\n");
        symbolicClientFinished(&client_finished, &shadow_client_finished);
    }
    else if (strcmp(rule, "HVR") == 0)
    {
        experimenttype = CLIENT;
        approach = SYMBEX;
        puts("Symbolically Executing HelloVerifyRequest:\n");
        symbolicHelloVerifyRequest(&hello_verify_request, &shadow_hello_verify_request);
    }
    else if (strcmp(rule, "SH") == 0)
    {
        experimenttype = CLIENT;
        approach = SYMBEX;
        puts("Symbolically Executing ServerHello:\n");
        symbolicServerHello(&server_hello, &shadow_server_hello);
    }
    else if (strcmp(rule, "SHD") == 0)
    {
        experimenttype = CLIENT;
        approach = SYMBEX;
        puts("Symbolically Executing ServerHelloDone:\n");
        symbolicServerHelloDone(&server_hello_done, &shadow_server_hello_done);
    }
    else if (strcmp(rule, "SCC") == 0)
    {
        experimenttype = CLIENT;
        approach = SYMBEX;
        puts("Symbolically Executing ChangeCipherSuite(C):\n");
        symbolicServerChangeCipher(&server_change_cipher, &shadow_server_change_cipher);
    }
    else if (strcmp(rule, "SFI") == 0)
    {
        experimenttype = CLIENT;
        approach = SYMBEX;
        puts("Symbolically Executing Finished(C):\n");
        symbolicServerFinished(&server_finished, &shadow_server_finished);
    }
    else if (strcmp(rule, "None") == 0)
    {
        approach = None;
        puts("Normal Execution!\n");
    }
    else
    {
        approach = None;
        printf("Rule is not defined!\n\n");
        printusage("./dtls-fuzz");
    }

#endif
    return 0;
}

void perform_psk_handshake(SSL *server, SSL *client, BIO *sinbio, BIO *soutbio, BIO *cinbio, BIO *coutbio, bool Isdump)
{

    unsigned char buf[4096];
    FILE *f;
    char fn[15];
    int r, c;
    c = 0;

    puts("\n[+] Now we start the PSK handshake:\n");

    do
    {

        if (!SSL_is_init_finished(client))
        {
            if (experimenttype == CLIENT || approach == None)
            {
                r = SSL_do_handshake(client);

                printf("client state: %s / %s\n", SSL_state_string(client),
                       SSL_state_string_long(client));

                if (r == -1 && (SSL_get_error(client, r) != SSL_ERROR_WANT_WRITE) &&
                    (SSL_get_error(client, r) != SSL_ERROR_WANT_READ))
                {
                    err();
                }
                r = BIO_read(coutbio, buf, 4096);
                if (r == -1)
                    err();
            }
            /////
            if (Isdump)
            {
                sprintf(fn, "%i", c);
                f = fopen(fn, "wb");
                fwrite(buf, 1, r, f);

                BIO_write(sinbio, buf, r);
                memset(buf, 0, sizeof(buf));
                c++;
            }
            else
            {
                if (c == 0)
                {
                    memset(buf, 0, sizeof(buf));
                    memcpy(buf, &client_hello1, CH0_size);
                    BIO_write(sinbio, buf, CH0_size);
                }

                else if (c == 2)
                {
                    memset(buf, 0, sizeof(buf));
                    memcpy(buf, &client_hello2, CH2_size);
                    BIO_write(sinbio, buf, CH2_size);
                }

                else if (c == 4)
                {
                    memset(buf, 0, sizeof(buf));
                    // Fragmentation

                    if (isFragmented)
                    {
                        memcpy(buf, &client_key_exchange_frag, CKE_frag_size);
                        memcpy(&buf[CKE_frag_size], &client_change_cipher, CCS_size);
                        memcpy(&buf[CKE_frag_size + CCS_size], &client_finished, FIN_size);

                        BIO_write(sinbio, buf, CKE_frag_size + CCS_size + FIN_size);
                    }
                    else
                    {
                        memcpy(buf, &client_key_exchange, CKE_unfrag_size);
                        memcpy(&buf[CKE_unfrag_size], &client_change_cipher, CCS_size);
                        memcpy(&buf[CKE_unfrag_size + CCS_size], &client_finished, FIN_size);

                        BIO_write(sinbio, buf, CKE_unfrag_size + CCS_size + FIN_size);
                    }
                }
                else if (c == 6)
                {
                    break;
                }

                memset(buf, 0, sizeof(buf));
                c++;
            }

            ////
        }
        else
        {
            printf("client state: %s / %s\n", SSL_state_string(client),
                   SSL_state_string_long(client));
        }

        if (!SSL_is_init_finished(server))
        {
            if (experimenttype == SERVER || approach == None)
            {
                printf("server state: %s / %s\n", SSL_state_string(server),
                       SSL_state_string_long(server));
                r = SSL_do_handshake(server);
                if ((r == -1) && (SSL_get_error(server, r) != SSL_ERROR_WANT_WRITE) &&
                    (SSL_get_error(server, r) != SSL_ERROR_WANT_READ))
                {
                    err();
                }

                r = BIO_read(soutbio, buf, 4096);
                if (r == -1)
                    err();
            }

            if (Isdump)
            {
                sprintf(fn, "%i", c);
                f = fopen(fn, "wb");
                fwrite(buf, 1, r, f);

                BIO_write(cinbio, buf, r);
                memset(buf, 0, sizeof(buf));
                c++;
            }
            else
            {
                if (c == 1)
                {
                    memset(buf, 0, sizeof(buf));
                    memcpy(buf, &hello_verify_request, HVR_size);
                    BIO_write(cinbio, buf, HVR_size);
                }
                else if (c == 3)
                {
                    memset(buf, 0, sizeof(buf));

                    if (isFragmented)
                    {
                        memcpy(buf, &server_hello_frag, SH_frag_size);
                        memcpy(&buf[SH_frag_size], &server_hello_done, SHD_size);
                        BIO_write(cinbio, buf, SH_frag_size + SHD_size);
                    }
                    else
                    {
                        memcpy(buf, &server_hello, SH_unfrag_size);
                        memcpy(&buf[SH_unfrag_size], &server_hello_done, SHD_size);
                        BIO_write(cinbio, buf, SH_unfrag_size + SHD_size);
                    }
                }
                else if (c == 5)
                {
                    memset(buf, 0, sizeof(buf));

                    memcpy(buf, &server_change_cipher, CCS_size);
                    memcpy(&buf[CCS_size], &server_finished, FIN_size);
                    BIO_write(cinbio, buf, CCS_size + FIN_size);
                }
                memset(buf, 0, sizeof(buf));
                c++;
            }

            printf("server state: %s / %s\n", SSL_state_string(server),
                   SSL_state_string_long(server));
        }
    } while ((!SSL_is_init_finished(server) || !SSL_is_init_finished(client)));
}

int main(int argc, char **argv)
{
    char rule[100];
    char cipher[20];
    int ret;
    int opt;
    int iarg = 0;
    int carg = 0;
    int rarg = 0;
    int narg = 0;
    ////////////////////////

    ////////////////////////////////////

    while ((opt = getopt(argc, argv, "i:r:nc:f")) != -1)
    {
        switch (opt)
        {
        case 'i':
            iarg = 1;
            if (sizeof(optarg) < 100)

                strcpy(dir_address, optarg);
            // strcpy(dir_address, "handshakes/frag");
            else
            {
                printf("Size of optarg exceeds the normal size\n");
                printusage(argv[0]);
            }
            break;

        case 'r':
            rarg = 1;
            strcpy(rule, optarg);
            // strcpy(rule, "None");
            break;

        case 'n':
            narg = 1;
            break;

        case 'c':
            carg = 1;
            // strcpy(cipher, optarg);
            strcpy(cipher, "psk");
            break;
        case 'f':
            isFragmented = 1;
            break;
        default:
            printusage(argv[0]);
            break;
        }
    }

    if (carg == 1)
    {
        int off = 0;
        off |= SSL_OP_NO_TICKET;

        SSL_library_init();
        SSL_load_error_strings();
        ERR_load_BIO_strings();
        OpenSSL_add_all_algorithms();

        ////////// Server Configuration
        // Variables
        SSL_CTX *sctx = NULL; // Server Context
        SSL *server = NULL;
        STACK_OF(OPENSSL_STRING) *ssl_args = NULL; // Needed for forcing the cipher suite
        //SSL_CONF_CTX *conf_ctx = NULL;             // We use config context to force cipher suite
        BIO *sinbio, *soutbio;
        ssl_args = sk_OPENSSL_STRING_new_null(); // Initializing the ssl arguments ()
        //conf_ctx = SSL_CONF_CTX_new();           // Initialize config context

        /////////////////////
        ////////// Client Configuration
        // Variables
        SSL_CTX *cctx = NULL;
        SSL *client = NULL;
        BIO *cinbio, *coutbio;

        if (strcmp(cipher, "psk") == 0)
        {

            // Server Initializations
            //SSL_CONF_CTX_set_flags(conf_ctx, SSL_CONF_FLAG_SERVER | SSL_CONF_FLAG_CMDLINE);
            // if (!sk_OPENSSL_STRING_push(ssl_args, "-cipher") ||
            //     !sk_OPENSSL_STRING_push(ssl_args, "PSK-AES128-CBC-SHA"))
            // {
            //     err();
            // } // Adding our preffered cipher suite to the context

            // if (!sk_OPENSSL_STRING_push(ssl_args, "-no_ticket"))
            // {
            //     err();
            // } // No session ticket will be issued by the server

            if (!(sctx = SSL_CTX_new(DTLSv1_server_method())))
                err();

            // if (!config_ctx(conf_ctx, ssl_args, sctx)) // Finilizing the process of forcing the cipher suite server
            //     err();

            SSL_CTX_set_options(sctx, off); // No Session Ticket

            SSL_CTX_set_psk_server_callback(sctx, psk_server_cb);

            if (!SSL_CTX_set_cipher_list(sctx, ciphersuite)) // OLD
            {
                err();
            }
            //SSL_CTX_set_verify(sctx, 0, verify_callback);

            //SSL_CTX_set_psk_find_session_callback(sctx, psk_find_session_cb);
            SSL_CTX_set_options(sctx, SSL_OP_COOKIE_EXCHANGE);     // Make Cookie Exchange Mandatory
            SSL_CTX_set_cookie_generate_cb(sctx, generate_cookie); // Cookie Generator
            SSL_CTX_set_cookie_verify_cb(sctx, verify_cookie);     // Cookie Verifier

            if (!(server = SSL_new(sctx)))
                err();

            //SSL_set_msg_callback(server, SSL_trace);
            //SSL_set_msg_callback_arg(server, BIO_new_fp(stdout, 0));
            ////
            sinbio = BIO_new(BIO_s_mem());
            soutbio = BIO_new(BIO_s_mem());
            SSL_set_bio(server, sinbio, soutbio);
            SSL_set_accept_state(server);

            // Client Initializations
            if (!(cctx = SSL_CTX_new(DTLSv1_client_method())))
                err();

            // if (!config_ctx(conf_ctx, ssl_args, cctx)) // Finilizing the process of forcing the cipher suite client
            //     err();

            SSL_CTX_set_options(cctx, off);
            SSL_CTX_set_psk_client_callback(cctx, psk_client_cb);
            //SSL_CTX_set_psk_use_session_callback(cctx, psk_use_session_cb);

            SSL_CTX_set_options(cctx, SSL_OP_COOKIE_EXCHANGE); // Make Cookie Exchange Mandatory
            if (!SSL_CTX_set_cipher_list(cctx, ciphersuite))
            {
                err();
            }

            if (!(client = SSL_new(cctx)))
                err();

            //SSL_set_msg_callback(client, SSL_trace);
            //SSL_set_msg_callback_arg(client, BIO_new_fp(stdout, 0));
            ////
            cinbio = BIO_new(BIO_s_mem());
            coutbio = BIO_new(BIO_s_mem());
            SSL_set_bio(client, cinbio, coutbio);
            SSL_set_connect_state(client);
            /////////////////////////////
            if (iarg == 1 && rarg == 1 && narg == 0)
            {
                if (((strcmp(rule, "bytecontain-server") == 0) || (strcmp(rule, "bytecontain-client") == 0) ||
                     (strcmp(rule, "mseqeq-server") == 0) || (strcmp(rule, "mseqeq-client") == 0) ||
                     (strcmp(rule, "mleneq-server") == 0) || (strcmp(rule, "mleneq-client") == 0)) &&
                    (isFragmented == 0))
                {
                    puts("Fragmentation should be enabled with this constraint!\n");
                    puts("[-] Add -f to proceed!\n");
                    printusage(argv[0]);
                }
                else if (((strcmp(rule, "bytecontain-server") != 0) && (strcmp(rule, "bytecontain-client") != 0) &&
                          (strcmp(rule, "mseqeq-server") != 0) && (strcmp(rule, "mseqeq-client") != 0) &&
                          (strcmp(rule, "mleneq-server") != 0) && (strcmp(rule, "mleneq-client") != 0)) &&
                         (isFragmented == 1))
                {
                    puts("Fragmentation cannot be enabled with this constraint!\n");
                    puts("[-] Remove -f to proceed!\n");
                    printusage(argv[0]);
                }

                dump_mode = 0;
                ret = load_psk_from_file(rule);
                if (ret == -1)
                {
                    goto failed;
                }
                perform_psk_handshake(server, client, sinbio, soutbio, cinbio, coutbio, dump_mode);
            }
            else if (iarg == 0 && rarg == 0 && narg == 1 && isFragmented == 0)
            {
                dump_mode = 1;
                perform_psk_handshake(server, client, sinbio, soutbio, cinbio, coutbio, dump_mode);
            }
            else
            {
                printusage(argv[0]);
            }
        }
        else if (strcmp(cipher, "ecc") == 0)
        {
        }
    }
    else
    {
        printusage(argv[0]);
    }

    ///////////////////////////////////

    return 0;

failed:
    puts("Something Went wrong!\n\n");
    exit(0);
}
